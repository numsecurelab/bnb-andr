package io.space.binancechainkit.core

import io.space.binancechainkit.models.Balance
import io.space.binancechainkit.models.LatestBlock
import io.space.binancechainkit.models.SyncState
import io.space.binancechainkit.models.Transaction

interface IStorage {
    var latestBlock: LatestBlock?
    var syncState: SyncState?

    fun setBalances(balances: List<Balance>)
    fun getBalance(symbol: String): Balance?
    fun getAllBalances(): List<Balance>?
    fun removeBalances(balances: List<Balance>)

    fun addTransactions(transactions: List<Transaction>)
    fun getTransactions(symbol: String, fromTransactionHash: String?, limit: Int?): List<Transaction>
}
